package com.baranov.bp.socketchatserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private final ServerSocket serverSocket;

    private InfoUsers userList = new InfoUsers();

    public Server(int port) throws IOException {
        this.serverSocket = new ServerSocket(port);
    }

    public void listen() throws IOException {

        while (true) {

            System.out.println("Wait for connection...");

            Socket clientSocket = this.serverSocket.accept();

            System.out.println("Client connected");

            //создание потока для нового подключения
            UserThread clientThead = new UserThread(clientSocket, userList);

            userList.getUserList().add(clientThead);

            clientThead.start();
        }
    }
}
