package com.baranov.bp.socketchatserver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class Client extends Thread implements Closeable {

    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;

    public Client(String ip, int port) throws IOException {
        this.socket = new Socket(ip, port);
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    public Socket getSocket() {
        return socket;
    }

    @Override
    public void run() {
        while (true) {
            try {
                //добавить проверку на закрыт ли сокет, если да - стопаем поток
                String message = retrieveMessage();
                System.out.println(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMassage(String message) throws IOException {
        this.writer.write(message.concat("\n"));
        this.writer.flush();
    }

    public String retrieveMessage() throws IOException {
        return this.reader.readLine();
    }

    @Override
    public void close() throws IOException {
        try {
            this.socket.close();
        } catch (IOException ignored) {
        }

        try {
            this.writer.close();
        } catch (IOException ignored) {
        }

        try {
            this.reader.close();
        } catch (IOException ignored) {
        }
    }
}
