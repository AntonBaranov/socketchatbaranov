package com.baranov.bp.socketchatserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ClientMain {

    public static void main(String[] args) throws IOException {

        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));

        // 127.0.0.1 or localhost for connections on your PC
        try (Client client = new Client("127.0.0.1", 8080)) {
            client.start();
            //while (client.getSocket().isConnected()) {
            while (true) {
                if (client.getSocket().isClosed()) {
                    client.close();
                    break;
                }
                String clientMessage = consoleReader.readLine();
                client.sendMassage(clientMessage);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
