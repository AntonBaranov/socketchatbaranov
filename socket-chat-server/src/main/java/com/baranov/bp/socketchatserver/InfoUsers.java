package com.baranov.bp.socketchatserver;


import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class InfoUsers {

    private List<UserThread> userList = new ArrayList<>();

    private Map<String, UserThread> userMap = new HashMap<>();

    public List<UserThread> getUserList() {
        return userList;
    }

    public Map<String, UserThread> getUserMap() {
        return userMap;
    }
}

