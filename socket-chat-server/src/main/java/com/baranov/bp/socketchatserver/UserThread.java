package com.baranov.bp.socketchatserver;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.net.Socket;
import java.util.Map;

public class UserThread extends Thread {

    private Socket clientSocket;
    private InfoUsers userList;
    private String login;
    private BufferedReader reader;
    private BufferedWriter writer;


    public UserThread(Socket clientSocket, InfoUsers userList) throws IOException {
        this.clientSocket = clientSocket;
        this.userList = userList;
        this.reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        this.writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
    }

    @Override
    public void run() {

        try {
            writer.write("Enter login:" + "\n");
            writer.flush();
            login = this.reader.readLine();
            writer.write("Type you message: " + "\n");
            writer.flush();
            userList.getUserMap().put(login, this);


            while (clientSocket.isConnected()) {
                String message = this.reader.readLine();

                if (message.equals("quit") || message.equals("q")) {
                    writer.write("Bye!");
                    writer.flush();
                    clientSocket.close();
                    userList.getUserList().remove(this);
                    userList.getUserMap().remove(login);
                    break;
                }

                for (Map.Entry<String, UserThread> entry : userList.getUserMap().entrySet()) {
                    UserThread userThread = entry.getValue();
                    String loginForSendMessage = entry.getKey();

                    if (loginForSendMessage != login) {
                        userThread.sendMessage("[" + login + "]: " + message);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            userList.getUserList().remove(this);
            userList.getUserMap().remove(login);
        }
    }

    public void sendMessage(String message) throws IOException {
        writer.write(message.concat("\n"));
        writer.flush();
    }
}
